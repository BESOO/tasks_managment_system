<?php

use App\Http\Controllers\NoteController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', function () {
    return view('login');
});

// Admin Features
Route::post('register', [UserController::class, 'register']);
Route::post('/user/create', [UserController::class, 'create_user']);
Route::post('/template/create', [TemplateController::class, 'create']);
Route::post('/project/create', [ProjectController::class, 'create']);
Route::middleware('admin')->post('/task/create', [TaskController::class, 'create']);

// User Features
Route::middleware('user')->post('/task/view', [TaskController::class, 'view']);
Route::middleware('user')->post('/task/complete', [TaskController::class, 'complete']);
Route::middleware('user')->post('/note/create', [NoteController::class, 'create']);


Route::get('/', [FrontendController::class, 'index']);
Route::post('/main/checklogin', [FrontendController::class, 'checklogin']);
Route::get('main/dashboard', [FrontendController::class, 'successlogin']);
Route::get('main/logout', [FrontendController::class, 'logout']);