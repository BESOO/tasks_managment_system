<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = "projects";

    protected $fillable = [
        'template_id',
        'user_id'
    ];

    public function template(){
        return $this->belongsTo(Template::class, 'template_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
