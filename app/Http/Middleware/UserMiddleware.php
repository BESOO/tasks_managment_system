<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(Auth::check()){
            if(Auth::user()->user_type == "user"){
                return $next($request);
            } else {
                return response()->json([
                    "message" => "You are not normal user!",
                ], 200);
            }
        } else {
            return response()->json([
                "message" => "You are not authenticated!",
            ], 200);
        }
    }
}
