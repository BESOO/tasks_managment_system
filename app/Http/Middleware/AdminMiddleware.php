<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(Auth::check()){
            if(Auth::user()->user_type == "admin"){
                return $next($request);
            } else {
                return response()->json([
                    "message" => "You are not admin!",
                ], 200);
            }
        } else {
            return response()->json([
                "message" => "You are not authenticated!",
            ], 200);
        }
        // return $next($request);
    }
}
