<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Auth;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum', 'admin']);
    }

    public function create(Request $request)
    {
        try {
            $user = Auth::user();
            $request->validate([
                'template_id' => 'required|exists:templates,id',
                'user_id' => 'required|exists:users,id',
            ]);

            $project = Project::create([
                'template_id' => $request->template_id,
                'user_id' => $request->user_id,
            ]);

            $project = Project::where('id', $project->id)->with('template')->with('user')->first();

            return response()->json([
                $project,
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
