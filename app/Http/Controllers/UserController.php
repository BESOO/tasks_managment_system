<?php

namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function register(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6'
            ]);

            DB::beginTransaction();
            $user = User::create([
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'username' => $request->username,
                'user_type' => "admin"
            ]);

            // $user->assignRole('admin');

            $user->save();

            DB::commit();

            $user->token = $user->createToken('mobile_app')->plainTextToken;
            return response()->json($user,  200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function create_user(Request $request)
    {
        try {

            $request->validate([
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6'
            ]);

            DB::beginTransaction();
            $user = User::create([
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'username' => $request->username,
                "user_type" => "user"
            ]);

            // $user->assignRole('user');

            $user->save();

            DB::commit();

            $user->token = $user->createToken('mobile_app')->plainTextToken;

            return response()->json($user, 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
