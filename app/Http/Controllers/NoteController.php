<?php

namespace App\Http\Controllers;

use App\Models\Note;
use App\Models\Task;
use Auth;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function create(Request $request)
    {
        try {
            $user = Auth::user();
            $request->validate([
                'note' => 'required|string',
                'task_id' => 'required|exists:tasks,id',
            ]);
            $task = Task::where('id', $request->task_id)->first();

            if ($task && $task->user_id == $user->id) {
                $note = Note::create([
                    'note' => $request->note,
                    'task_id' => $request->task_id,
                    'user_id' => $user->id
                ]);
                $note = Note::where('id', $note->id)->with('task')->with('user')->first();

                return response()->json([
                    $note,
                ], 200);
            } else {
                return response()->json([
                    "message" => "This task is not yours!",
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
