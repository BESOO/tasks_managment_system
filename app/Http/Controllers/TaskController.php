<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function create(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required|string',
                'description' => 'required|string',
                'due_date' => 'required',
                'project_id' => 'required|exists:projects,id',
                'user_id' => 'required|exists:users,id',
            ]);
            $user = User::where('id', $request->user_id)->first();
            if ($user->user_type == "user") {
                $task = Task::create([
                    'title' => $request->title,
                    'description' => $request->description,
                    'due_date' => date("Y-m-d", strtotime($request->due_date)),
                    'user_id' => $request->user_id,
                    'project_id' => $request->project_id,
                ]);
                $task = Task::where('id', $task->id)->with('user')->with('project')->first();

                return response()->json([
                    $task,
                ], 200);
            } else {
                return response()->json([
                    "message" => "You cannot sign the task into admin user!",
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function view(Request $request)
    {
        try {
            $user = Auth::user();

            $request->validate([
                'due_date' => 'required|date',
            ]);

            $tasks = Task::where('due_date', '=', $request->due_date)
                ->where('user_id', $user->id)
                ->with('user')
                ->with('project')->get();

            return response()->json($tasks, 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function complete(Request $request)
    {
        try {
            $user = Auth::user();
            $request->validate([
                'task_id' => 'required|exists:tasks,id',
            ]);

            $user = Auth::user();
            $task = Task::where('id', $request->task_id)->where('user_id', $user->id)->first();
            if ($task) {
                Task::where('id', $request->task_id)->update(['status' => 'completed']);
                return response()->json([
                    "message" => "The task has been marked as completed!",
                ], 200);
            } else {
                return response()->json([
                    "message" => "This task is not yours!",
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
