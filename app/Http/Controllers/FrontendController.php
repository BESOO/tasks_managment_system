<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function checklogin(Request $request)
    {
        echo "Asdasdasdsa";
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:6',
        ]);

        $user_data = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        );

        echo "Asdasdasdasdas";
        if (Auth::attempt($user_data)) {
            echo "successlogin";
            return redirect('api/main/dashboard');
        } else {
            echo "faaaaiiill";
            return back()->with('error', 'Wrong Login Details');
        }

    }

    public function successlogin()
    {
        return view('main/dashboard_page');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('main');
    }
}
