<?php

namespace App\Http\Controllers;

use App\Models\Template;
use Auth;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum', 'admin']);
    }
    
    public function create(Request $request){
        try {
            $user = Auth::user();
            $request->validate([
                'title' => 'required|string'
            ]);

            $template = Template::create([
                'title' => $request->title
            ]);

            return response()->json([
                $template,
            ], 200); 

        } catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
